package com.example.appzlogic.hit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by APPZLOGIC on 5/4/2016.
 */
public class Splash extends Activity
{
    private final int SPLASH_TIME = 4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);

    new Handler().postDelayed(new Runnable() {
    @Override
    public void run() {

            Intent loginPageIntent = new Intent(Splash.this, MainActivity.class);
            startActivity(loginPageIntent);
            finish();
            Splash.this.finish();
                        }
    }, SPLASH_TIME);
}
}

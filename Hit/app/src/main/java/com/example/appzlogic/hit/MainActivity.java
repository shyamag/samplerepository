package com.example.appzlogic.hit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.appzlogic.hit.curl.CurlActivity;

public class MainActivity extends AppCompatActivity {

        private ListView listView;
        private String names[] = {
                "HIT Sudha Apr-2016",
                "HIT Sudha May-2016",
                "HIT Sudha Jun-2016",
                "HIT Sudha Jul-2016"
        };

        private String desc[] = {
                "The Power of Holy books",
                "The Power of Holy River",
                "The Power of Holy Earth",
                "The Power of GOD"
        };


        private Integer imageid[] = {
                R.drawable.book,
                R.drawable.books,
                R.drawable.bookin,
                R.drawable.book
        };


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            CustomList customList = new CustomList(this, names, desc, imageid);

            listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(customList);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                  Intent intent=new Intent(MainActivity.this,CurlActivity.class);
                    startActivity(intent);
                }
            });
        }
}
